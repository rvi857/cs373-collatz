#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from typing import IO, List

#cycle length cache storing common cycle lengths, in the form of a python dictionary:
cycache = dict()

# ------------
# collatz_read
# ------------

def collatz_read (s: str) -> List[int] :
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]

# ------------
# collatz_eval
# ------------

def collatz_eval (i: int, j: int) -> int :
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """

    """
    Preconditions:
    - Both arguments must be integers
    - Both arguments must be greater than 0
    - Both arguments should not be equal, as they represent a range
    - The second argument must be greater than the first
    """
    assert isinstance(i, int)
    assert isinstance(j, int)
    assert i > 0 and i < 1000000
    assert j > 0 and j < 1000000

    #account for condition that i < j by swapping i and j
    if i > j:
        i, j = j, i

    assert j >= i

    cymax = 0
    for x in range(i, j):
        if x in cycache:
            cylen = cycache[x]
        else:
            cylen = collatz_cycle_length(x)
        if cylen > cymax:
            cymax = cylen

    """
    Postconditions:
    - Result must be an integer
    - Result must be >= 1
    """
    assert isinstance(cymax, int)
    assert cymax >= 1

    return cymax

# ------------
# collatz_cycle_length
# ------------

def collatz_cycle_length (x: int) -> int :
    """
    read int
    return cycle length of int
    """

    """
    Preconditions:
    - Argument must be an integer
    - Argument must be greater than 0
    - Argument not stored in cache of cycle lengths
    """
    assert isinstance(x, int)
    assert x >= 1

    #storing initial argument before iteration for storage in cache later
    initial = x

    #cycle length includes ending 1, which isn't counted in loop
    cylen = 1

    #keep iterating through algorithm until x == 1
    while x != 1:
        if (x % 2) == 0:
            x = x // 2
        else:
            x = 3 * x + 1
        if x in cycache:
            cylen += cycache[x]
            break
        else:
            cylen += 1

    """
    Postconditions:
    - Result must be an integer
    - Result must be >= 1
    - Result must be added to cache
    """
    assert isinstance(cylen, int)
    assert cylen >= 1

    cycache[initial] = cylen 
    return cylen

# -------------
# collatz_print
# -------------

def collatz_print (w: IO[str], i: int, j: int, v: int) -> None :
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")

# -------------
# collatz_solve
# -------------

def collatz_solve (r: IO[str], w: IO[str]) -> None :
    """
    r a reader
    w a writer
    """
    for s in r :
        i, j = collatz_read(s)
        v    = collatz_eval(i, j)
        collatz_print(w, i, j, v)
        